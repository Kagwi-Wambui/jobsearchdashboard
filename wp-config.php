<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '@c`;#hDn&g8fNzr)~o#3ENv.)ZQq,//>/}TNeNP940qCWN>QZMJ#a8x+B_|h(L=/' );
define( 'SECURE_AUTH_KEY',  '+^efvmX_Mq-sH@[Qi1uH +mjk3m@pZ.Y/`:?XNv.a(2G#B(`)RnkYLH,l0l5-^&m' );
define( 'LOGGED_IN_KEY',    'kY$wSaAT+Y>0bcUr+3{`1|+<.S2E0vbz&UMj <tS:Zaq?%<DV4[sk /tB31H4;-C' );
define( 'NONCE_KEY',        'OE9M J6j{+Ane#hS,k1z;zwzbSM8`duFZD8P#=B#[0K86|{X2-{eU{&3kBY)l$o~' );
define( 'AUTH_SALT',        'd:iYAZ}-S%odB; )-1J0R${%j%o_ufPIH|QQb}G,f_NvD#zAGB}89S8Aq1*4/dY^' );
define( 'SECURE_AUTH_SALT', '*3lO?X/#8tjUuX86C.9f&YX0t(H%(dp#TL2L6cBv^x_t&l8V)cq[lo4f}XM67)K*' );
define( 'LOGGED_IN_SALT',   'T$&fkV&b[/#,pvvd`eU,k)oc-e IaIXT&fswNz.Cd^FVa>Mi{RVRq-]AWTufx[Gu' );
define( 'NONCE_SALT',       'lmg#ul0nXCun^@5D@Ydv!G]~6M!sX0ly)yJM(S3<aHum~A3%Q=^7nZEt4k$-85P>' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
